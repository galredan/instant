import firebase from "firebase";

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyBHr2mrmdORK21_GnT8XxqPRVUh0oV5CNk",
    authDomain: "instantclone-2134c.firebaseapp.com",
    projectId: "instantclone-2134c",
    storageBucket: "instantclone-2134c.appspot.com",
    messagingSenderId: "793554303166",
    appId: "1:793554303166:web:a8aad29eef2113dd09c6ef"
  };
 
  try {
    firebase.initializeApp(firebaseConfig);
  } catch (err) {
    if (!/already exists/.test(err.message)) {
      console.error('Firebase initialization error', err.stack);
    }
  }
  const fire = firebase;
  export default fire;

  