import axios from "axios";

import fire from '../fire';

const url = 'http://localhost:3001/posts'
<<<<<<< HEAD
=======
const likeUrl = 'http://localhost:3001/posts/like'
>>>>>>> dev

const createToken = async () => {

    const user = fire.auth().currentUser;
    const token = user && (await user.getIdToken());

    const payloadHeader = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        }
    };

    return payloadHeader;
}

export const addPost = async (content, name) => {
    const header = await createToken();
    console.log(content, name)
    const payload ={
        content,
        name
    }
    try {
        const res = await axios.post(url, payload, header);
        return res.data;
    }catch (e) {
        console.error(e);
    }
    
}

export const getPosts = async () => {
    const header = await createToken();

    try {
        const res = await axios.get(url, header)
        return res.data;
    } catch (e) {
        console.error(e);
    }
}

export const like = async (name, post) => {
    const header = await createToken();
    var isAlreadyLiked = false;
    var index = 0;

    post.like.map((likeName, likeIndex) => {
        if(likeName === name){
            isAlreadyLiked = true
            index = likeIndex
        }
    })
    
    isAlreadyLiked ? removeItemOnce(post.like, name) : post.like.push(name)
    const payload ={
        id: post.id,
        content: post.content,
        name: post.name,
        like: post.like
    }
    try {
        const res = await axios.put(likeUrl, payload, header);
        return res.data;
    }catch (e) {
        console.error(e);
    }
}

function removeItemOnce(arr, value) {
    var index = arr.indexOf(value);
    if (index > -1) {
      arr.splice(index, 1);
    }
    return arr;
}


